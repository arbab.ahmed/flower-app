package com.arbab.flowerapp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class AppUtils {

    public static Object parseJSONObject(String response, Class<?> className) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(jsonObject.toString(), className);
    }
}
