package com.arbab.flowerapp.activities;

import androidx.annotation.IdRes;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.os.Handler;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.arbab.flowerapp.fragments.GiftFragment;
import com.arbab.flowerapp.fragments.HomeFragment;
import com.arbab.flowerapp.fragments.ProfileFragment;
import com.arbab.flowerapp.fragments.SearchFragment;
import com.arbab.flowerapp.R;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

public class HomeActivity extends AppCompatActivity {

    HomeFragment homeFragment;
    SearchFragment searchFragment;
    ProfileFragment profileFragment;
    GiftFragment giftFragment;
    FrameLayout fragment_container;
    BottomBar bottomBar;
    boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        homeFragment = new HomeFragment();
        giftFragment = new GiftFragment();
        searchFragment = new SearchFragment();
        profileFragment = new ProfileFragment();
        fragment_container = findViewById(R.id.contentContainer);

        bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if (tabId == R.id.tab_home) {
                    setFragment(homeFragment, false);
                } else if (tabId == R.id.tab_search) {
                    setFragment(searchFragment, true);
                } else if (tabId == R.id.tab_gift) {
                    setFragment(giftFragment, true);
                } else if (tabId == R.id.tab_profile) {
                    setFragment(profileFragment, true);
                }
            }
        });


        Toolbar toolbar = (Toolbar)findViewById(R.id.navAction);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);

        toolbar.setTitle("Home");







    }

    protected void setFragment(Fragment fragment, boolean addToStack) {
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.contentContainer, fragment);
        if (addToStack) {
            t.addToBackStack("");
        }
        t.commit();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            System.exit(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }
}
