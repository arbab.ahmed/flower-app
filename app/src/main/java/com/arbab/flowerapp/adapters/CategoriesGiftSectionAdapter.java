package com.arbab.flowerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.arbab.flowerapp.R;
import com.arbab.flowerapp.models.ProductsModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import render.animations.Bounce;
import render.animations.Render;


public class CategoriesGiftSectionAdapter extends RecyclerView.Adapter<CategoriesGiftSectionAdapter.ProductViewHolder> {
    private List<ProductsModel.Product> productList;
    private Context context;
    Render render;


    public CategoriesGiftSectionAdapter(List<ProductsModel.Product> restaurantList, Context context) {
        this.productList = restaurantList;
        this.context = context;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.categories_gift_section_list_item, parent, false);

        render = new Render(context);


        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {

        final ProductsModel.Product product = productList.get(position);


        if (position % 2 == 1) {
            render.setAnimation(Bounce.InLeft(holder.parentLayout));
            render.start();
        } else {
            render.setAnimation(Bounce.InRight(holder.parentLayout));
            render.start();
        }


        Picasso.get().load(product.getImages().get(0).getSrc()).placeholder(R.drawable.logo).into(holder.imageView);
        holder.productTitle.setText(product.getName());
        holder.productDesc.setText(product.getShortDescription());

    }

    @Override
    public int getItemCount() {

        if (productList != null) {
            return productList.size();
        } else {
            return 0;
        }
    }


    public class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView productTitle,productDesc;
        ImageView imageView;
        CardView parentLayout;


        public ProductViewHolder(View itemView) {
            super(itemView);
            productTitle = (TextView) itemView.findViewById(R.id.product_name);
            productDesc = (TextView) itemView.findViewById(R.id.product_description);
            imageView = (ImageView) itemView.findViewById(R.id.imageViewItem);
            parentLayout = itemView.findViewById(R.id.parent_layout);

        }


    }
}

